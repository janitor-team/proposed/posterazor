posterazor (1.5.1-8) unstable; urgency=medium

  * Renamed the executable to provide lowercase command. Consequently:
      - Renamed the following files:
          ~ debian/manpage/PosteRazor.1 to debian/manpage/posterazor.1
          ~ debian/manpage/PosteRazor.txt to debian/manpage/posterazor.txt
      - Updated references at following files:
          ~ debian/appstream/io.sourceforge.posterazor.desktop
          ~ debian/lintian-overrides
          ~ debian/manpage/create-man.sh
          ~ debian/manpages
          ~ debian/tests/control
      - debian/links: created to provide backward compatibility with the
        camelcase command.
      - debian/rules: added override_dh_install to rename the binary.
  * debian/manpage/*: updated files to modify the manpage's layout.
  * debian/patches/:
      - 010_ftbfs-werror-format-security.patch:
          ~ Added the 'Bug-Debian' field in header.
          ~ Updated the 'Origin' field.
      - 020_ftbfs-gcc-4.7.patch:
          ~ Added the 'Bug-Debian' field.
          ~ Removed the 'Origin' field from header due the patch is attached
            at the bug's thread.
  * debian/source/lintian-overrides: created to override messages about manpage
    and .desktop files, as requested by lintian.

 -- Marcelo Soares Mota <motasmarcelo@gmail.com>  Sun, 12 Jul 2020 13:13:49 -0300

posterazor (1.5.1-7) unstable; urgency=medium

  * New maintainer. Thanks to Simrun Basuita for previous work.
    (Closes: #866220)
  * Converted manpage source from docbook to txt2man's system. Consequently:
      - Manpage rewritten from scratch.
      - debian/clean: removed due no longer needed (old) manpage's deletion.
      - debian/control: removed no longer needed docbook* build dependencies.
      - debian/copyright: added copyright to create-man.sh.
      - debian/manpage/*: created files to produce the manpage.
      - debian/manpages: updated to install the new manpage.
      - debian/PosteRazor.1.docbook: removed.
      - debian/rules: removed no longer needed override_dh_installman.
  * Implemented AppStream metainfo XML files format. Consequently:
      - Created debian/appstream/ to group all related files.
      - debian/appstream/icons/: created to provide new PNG icons.
      - debian/appstream/io.sourceforge.posterazor.metainfo.xml: created.
      - debian/copyright: added copyright to the metainfo XML file.
      - debian/install:
          - Added debian/appstream/ files entries.
          - Removed the XPM icon entry to comply with AppStream specs.
      - debian/posterazor.desktop:
          ~ Renamed to io.sourceforge.posterazor.desktop.
          ~ Moved to debian/appstream/.
          ~ Updated the 'Icon' and 'Categories' fields.
      - debian/source/include-binaries: added the new PNG icons entries.
  * debian/control: improved the long description to add more features.
  * debian/patches/:
      - All patches: submitted to upstream and added the 'Forwarded' field to
        their headers.
      - 010_ftbfs-werror-format-security.patch: added 'Author' and 'Last-Update'
        fields in header.
      - 040_add-cmake-minimum-required-command.patch: created to comply with
        CMP0000 CMake's policy.

 -- Marcelo Soares Mota <motasmarcelo@gmail.com>  Mon, 29 Jun 2020 21:51:48 -0300

posterazor (1.5.1-6) unstable; urgency=medium

  * QA upload.
  * debian/control: updated VCS fields to use Salsa.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.

 -- Marcelo Soares Mota <motasmarcelo@gmail.com>  Mon, 08 Jun 2020 19:19:04 -0300

posterazor (1.5.1-5) unstable; urgency=medium

  * QA upload.
  * Migrations:
      - debian/copyright:
          ~ Migrated to 1.0 format.
          ~ Updated all data.
      - debian/rules to new (reduced) format. Consequently:
          ~ Added hardening flags.
          ~ Created the following files:
              + debian/install.
              + debian/manpages.
  * debian/clean: added to remove files created by upstream.
  * debian/control:
      - Improved long description to mention some supported file types.
      - Updated the Homepage field.
  * debian/dirs: removed due creation of debian/install.
  * debian/lintian-overrides: added to override false positives about
    spelling errors in foreign translate files.
  * debian/patches/:
      - Added 030_fix-spelling-binary.patch to fix spelling error in English
        translation file.
      - Added a numeric prefix for all patches.
  * debian/posterazor.desktop: added 'Keywords' field.
  * debian/posterazor.menu: removed to comply to debian policy because
    a '.desktop' file already exists.
  * debian/rules:
      - Added DH parameters to fix FTCBFS by passing cross flags to cmake.
        Thanks to Helmut Grohne <helmut@subdivi.de>. (Closes: #888445)
      - Added the override_dh_installman target to build and install manpage.
  * debian/upstream/metadata: created.
  * debian/watch:
      - Bumped to version 4.
      - Changed search rule according the new SourceForge template.

 -- Marcelo Soares Mota <motasmarcelo@gmail.com>  Thu, 28 May 2020 13:42:02 -0300

posterazor (1.5.1-4) unstable; urgency=medium

  * QA upload.
  * Ran wrap-and-sort.
  * debian/control: added 'Rules-Requires-Root: no' to source stanza.
  * debian/source/include-binaries: created to include image files used
    in CI tests.
  * debian/tests/*: created to provide some CI tests.

 -- Marcelo Soares Mota <motasmarcelo@gmail.com>  Fri, 22 May 2020 12:09:51 -0300

posterazor (1.5.1-3) unstable; urgency=medium

  * QA upload.
  * Set Debian QA Group as maintainer. (see #866220)
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control: bumped Standards-Version to 4.5.0.
  * debian/rules: changed dh_clean to dh_prep in 'install' target.

 -- Marcelo Soares Mota <motasmarcelo@gmail.com>  Mon, 11 May 2020 22:49:47 -0300

posterazor (1.5.1-2) unstable; urgency=low

  * Fix FTBFS with GCC 4.7 (Closes: #667328)
  * Fix FTBFS with -Werror=format-security (Closes: #653824)
  * Add debian/menu (Closes: #627920)
  * Rewrite package descriptions

 -- Simrun Basuita <simrunbasuita@gmail.com>  Sun, 20 May 2012 18:02:42 +0100

posterazor (1.5.1-1) unstable; urgency=low

  * Initial release (Closes: #545789)

 -- Simrun Basuita <simrunbasuita@googlemail.com>  Sun, 13 Dec 2009 15:01:25 +0000
